package com.example.javatest;

import java.util.Arrays;

// Test Java No.2
public class testJavaTwo {

    public static void main(String[] args) {

        int[] nums = {1,2,3,4};
        System.out.println(Arrays.toString(nums));

        int[] x = Arrays.copyOf(nums, 3);
        System.out.println(Arrays.toString(nums));
        System.out.println(Arrays.toString(x));

    }
}
