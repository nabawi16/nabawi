package com.example.javatest;

import java.util.Arrays;

// Test Java No.1
public class testJavaOne {

    public static void main(String[] args) {
        int[] arrayNums = {3,1,4,2};
        System.out.println(Arrays.toString(arrayNums));

        for (int i = 0; i < 4; i++){
            System.out.println("index number to"+i+"is =" + arrayNums[i]);
        }

        System.out.println(arrayNums.length);
    }
}
